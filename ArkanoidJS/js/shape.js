function RectGradient()
{
	// Create gradient
	var grd = ctx.createRadialGradient(75,50,5,90,60,100);
	grd.addColorStop(0,"red");
	grd.addColorStop(1,"white");

	// Fill with gradient
	ctx.fillStyle = grd;
	ctx.fillRect(10,10,150,80);
}

function DrawRect(){
	ctx.fillStyle = "#F000F0";
	ctx.fillRect(0,0,150,75);
}

function DrawImage() {
    var img = document.getElementById("scream");
    ctx.drawImage(img,10,10);
}

function DrawText(){
	ctx.font = "30px Arial";
	ctx.strokeText("Hello World",10,50);
}

function DrawText(){
	ctx.font = "30px Arial";
	ctx.fillText("Hello World",10,50);
}

function DrawColoredText()
{
	ctx.font = "30px Comic Sans MS";
	ctx.fillStyle = "red";
	ctx.textAlign = "center";
	ctx.fillText("Hello World", canvas.width/2, canvas.height/2); 
}

function DrawFilledText()
{
	ctx.beginPath();
	ctx.arc(95,50,40,0,2*Math.PI);
	ctx.stroke();
}

function DrawLine()
{
	ctx.moveTo(0,0);
	ctx.lineTo(200,100);
	ctx.stroke();
}