﻿function PowerUp(x, y, w, h) {
    var self = this;
    this.type = "powerup";
    this.size = { x: 50, y: 30 };
    this.position = { x: x, y: y };
    this.powerUpImg = new Image();
    this.powerUpImg.src = "images/powerup_fastball.png";
    this.alive = true;
    this.velocity = 0.8;
    this.age = 0;

    this.velocityIncrease = 1.3;

    this.toString=function(){ 
        return '[powerup "'+this.type+'"]';
    } 

    this.Update = function (step) {
        var x = 3 * Math.sin(this.age * 0.05);
        this.age++;

        //move down in sine wave
        this.position.x += x;
        this.position.y += this.velocity;
    };

    this.OnDeath = function () {
        if (this.alive)
            console.log("powerup destroyed");
        this.alive = false;
    }

    this.OnCollide = function (collider) {

    }

    this.UpdateCollision = function () {
        if (this.position.y > canvasHeight)
            this.OnDeath();
    }

    this.Draw = function () {
        ctx.drawImage(this.powerUpImg, this.position.x, this.position.y, this.size.x, this.size.y);
    };
}