﻿function PowerUpFastBall(x, y) {
    var self = this;
    this.type = "powerup";
    this.size = { x: 50, y: 30 };
    this.position = { x: x, y: y };
    this.powerUpImg = new Image();
    this.powerUpImg.src = "images/powerup_fastball.png";
    this.alive = true;
    this.velocity = 0.8;
    this.age = 0;

    this.velocityIncrease = 1.3;

    this.Update = function (step) {
        var x = 3 * Math.sin(this.age * 0.05);
        this.age++;

        //move down in sine wave
        this.position.x += x;
        this.position.y += this.velocity;
    };

    this.OnDeath = function () {
        //if (this.alive)
        //    console.log("powerup destroyed");
        this.alive = false;
    }

    this.OnCollide = function (collider) {

    }

    this.UpdateCollision = function () {
        if (this.position.y > canvasHeight)
            this.OnDeath();

        //has collision with player
        currentState.gameObjects.forEach(function (go) {
            if (self.type != go.type && go.type == "player") {
                if (HasBoxCollision(self, go)) {
                    self.OnDeath();

                    //apply speed to all bals
                    currentState.gameObjects.forEach(function (go) {
                        if (go.type == "ball") {
                            go.velocity.x *= self.velocityIncrease;
                            go.velocity.y *= self.velocityIncrease;
                        }
                    });

                    go.OnCollide(self);
                }
            }
        });
    }

    this.Draw = function () {
        ctx.drawImage(this.powerUpImg, this.position.x, this.position.y, this.size.x, this.size.y);
    };
}