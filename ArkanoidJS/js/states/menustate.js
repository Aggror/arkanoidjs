﻿function MenuState() {

    this.name = "MENUState";
    this.startButton;

    this.InitState = function () {
        var startBtnWidth = 260;
        var textX = (canvasWidth / 2) - (startBtnWidth / 2);
        var textY = (canvasHeight / 2);
        startButton = new Button("Start game", textX, textY, startBtnWidth, 80);
        startButton.OnCliked = function () {
            console.log("Start the game");

            currentState.ExitState();
            currentState = new GameState();
            currentState.InitState();
        };
    }

    this.UpdateState = function() {

    }

    this.DrawState = function() {
        //Draw Text
        //text position
        var text = "Arkanoid JS";
        var fontSize = 80;
        ctx.font = fontSize + "px Verdana";
        ctx.fillStyle = "#FFFF00";
        var textSize = ctx.measureText(text);
        var textX = (canvasWidth / 2) - (textSize.width / 2);
        var textY = (canvasHeight / 3) - (fontSize / 2);
        
        ctx.fillText("Arkanoid JS", textX, textY);

        startButton.Draw();

    }

    this.ExitState = function() {

    }
};