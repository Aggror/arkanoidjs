﻿function GameState() {
    this.player;
    this.ballCount = 0;
    this.gameObjects = [];
    this.gameOver = false;
    this.gameOverBtn;

    this.InitState = function () {
        //Init game Elements
        this.player = new Player(canvasWidth / 2 - 60, canvasHeight - 40);
        var ball = new Ball();
        ball.position.x = (canvasWidth / 2 - 60);
        ball.position.y = (canvasHeight - 120);
        this.ballCount++;

        this.gameObjects.push(this.player);
        this.gameObjects.push(ball);

        //Blocks
        var tilesInWidth = 10, tilesInWidth, blockHeight = 20, offset = 8;
        var blockHeight = 20, blockWidth = (canvasWidth - (10 * offset)) / 9;
        for (var i = 0; i < tilesInWidth; i++) {
            for (var j = 0; j < 6; j++) {
                var block = new Block();
                block.position = { x: i * (blockWidth + offset) + offset, y: j * (blockHeight + offset) + 2 * offset }
                block.size = { x: blockWidth, y: blockHeight };
                block.LoadImageOfColor(j);
                this.gameObjects.push(block);
            }
        }
    }

    this.CheckForGameOver = function () {
        if (this.ballCount <= 0) {
            this.player.ignoreCollision = true;
            this.gameOver = true;
            var startBtnWidth = 300;
            var textX = (canvasWidth / 2) - (startBtnWidth / 2);
            var textY = (canvasHeight / 2);
            this.gameOverBtn = new Button("Restart game", textX, textY, startBtnWidth, 80);
            this.gameOverBtn.OnCliked = function () {
                console.log("Start the game");

                currentState.ExitState();
                currentState = new GameState();
                currentState.InitState();
            };
        }
    }

    this.UpdateState = function (step) {
        //Update all gameobjects
        for (var i = 0; i < this.gameObjects.length; i++) {
            this.gameObjects[i].Update(step);
            if (!this.gameObjects[i].alive) {
                this.gameObjects.splice(i, 1);
            }
        }

        //Update all Collisions of gameobjects
        for (var i = 0; i < this.gameObjects.length; i++) {
            this.gameObjects[i].UpdateCollision();
        }
    }

    this.DrawState = function () {
        //Draw all gameobjects
        for (var i = 0; i < this.gameObjects.length; i++) {
            this.gameObjects[i].Draw();
        }

        if (this.gameOver) {
            this.gameOverBtn.Draw();
        }
    }

    this.ExitState = function () {
        //clear gameobjects array
        this.gameObjects.length = 0;
    }
};