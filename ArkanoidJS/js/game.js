"use strict";

var cv, ctx;
var FPS = 30, canvasWidth = 800, canvasHeight = 600;
var currentState;

$(document).ready(function () {

    //Init some important elements
    cv = document.getElementById("myCanvas");
    ctx = cv.getContext("2d");
    currentState = new MenuState();
    currentState.InitState();

    //GAME LOOP
    var now,
    dt = 0,
    last = timestamp(),
    step = 1 / 60;
    function frame() {
        now = timestamp();
        dt = dt + Math.min(1, (now - last) / 1000);
        while (dt > step) {
            dt = dt - step;
            Update(step);
        }
        Draw(dt);
        last = now;
        requestAnimationFrame(frame);
    }
    requestAnimationFrame(frame);


    function Update(step) {
        currentState.UpdateState(step);
    }

    function Draw() {
        //clear
        ctx.clearRect(0, 0, canvasWidth, canvasHeight);

        //Draw Background
        ctx.fillStyle = "#000000";
        ctx.fillRect(0, 0, canvasWidth, canvasHeight);

        //draw currentState
        currentState.DrawState();
    }
});