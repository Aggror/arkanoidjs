﻿function Ball() {
    var self = this;
    this.type = "ball";
    this.size = { x: 20, y: 20 };
    this.radius = this.size.x / 2;
    this.position = { x: 0, y: 0 };
    this.center = { x: this.position.x + this.radius, y: this.position.y + this.radius };
    this.velocity = { x: 5, y: -3.7812 };
    this.ballImg = new Image();
    this.ballImg.src = "images/ballGrey.png";
    this.alive = true;

    this.Update = function () {
        if (input.space) {
            this.velocity.x += 1;
            this.velocity.y += 1;
        }

        this.position.x += this.velocity.x;
        this.position.y += this.velocity.y;
        this.center = { x: this.position.x + this.radius, y: this.position.y + this.radius };
    }

    this.RevertX = function () {
        this.velocity.x = -this.velocity.x;
    }
    this.RevertY = function () {
        this.velocity.y = -this.velocity.y;
    }

    this.OnDeath = function () {
        if (this.alive)
            console.log("you died");
        this.alive = false;
        currentState.ballCount--;
        currentState.CheckForGameOver();
    }

    this.UpdateCollision = function () {
        //edges
        if (this.position.x <= 0)
            this.RevertX();
        else if (this.position.x + this.size.x >= canvasWidth)
            this.RevertX();
        if (this.position.y <= 0)
            this.RevertY();
        else if (this.position.y + this.size.y >= canvasHeight) {
            this.OnDeath();
        }
        currentState.gameObjects.forEach(function (go) {
            if (self.type != go.type) {
                if (HasBoxCollision(self, go)) {
                    go.OnCollide(self);
                    if(go.type == "block")
                        self.velocity.y = Math.abs(self.velocity.y);
                }
            }
        });
    }

    this.Draw = function () {
        ctx.drawImage(this.ballImg, this.position.x, this.position.y, this.size.x, this.size.y);
        //ctx.drawImage(this.ballImg, 0, 0, this.size, this.size);
    };
}