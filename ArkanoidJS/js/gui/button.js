﻿var Button = function (text, x, y, width, height) {
    var self = this;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.clicked = false;
    this.hovered = false;
    this.text = text;
    this.OnClicked;

    cv.addEventListener('click', function (e) {

        //console.log("clicked: "+ e.x + "__" + e.y);
        //console.log("buttonP: " + self.x + "__" + self.y);

        var rect = cv.getBoundingClientRect();
        var mPos = {};
        mPos.x = e.x - rect.left;
        mPos.y = e.y - rect.top;
        //console.log("adjust: " + mPos.x + "__" + mPos.y);

        //clicked?
        if (mPos.x >= self.x && mPos.x <= x + width && mPos.y >= self.y && mPos.y <= self.y + height) {
            //console.log("clicked");
            self.OnCliked();
        }
    });
}

Button.prototype.Draw = function() {
    //draw button 
    ctx.fillStyle = "#FFFF00";
    ctx.fillRect(this.x, this.y, this.width, this.height);

    //text options
    var fontSize = 40;
    ctx.fillStyle = "#FF0000";
    ctx.font = fontSize + "px Verdana";

    //text position
    var textSize = ctx.measureText(this.text);
    var textX = this.x + (this.width/2) - (textSize.width / 2);
    var textY = this.y + (this.height / 2) + (fontSize/2);

    //draw the text
    ctx.fillText(this.text, textX, textY);
}