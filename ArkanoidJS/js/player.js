﻿function Player(startPosX, startPosY) {
    var self = this;
    this.type = "player";
    this.size = { x: 120, y: 20 };
    this.position = { x: startPosX, y: startPosY };
    this.moveSpeed = 10;
    this.padSpeed = 0;
    this.inertia = 0.90;
    this.playerImg = new Image();
    this.playerImg.src = "images/paddleBlu.png";
    this.alive = true;
    this.ignoreCollision = false;

    this.Update = function () {
        //Key input
        if (input.left)
            self.padSpeed = -self.moveSpeed;
        else if (input.right)
            self.padSpeed = self.moveSpeed;

        //Update position
        this.position.x += this.padSpeed;
        this.padSpeed *= this.inertia;
        if (this.position.x < 0)
            this.position.x = 0;
        else if (this.position.x + this.size.x > canvasWidth)
            this.position.x = canvasWidth - this.size.x;
    };

    this.UpdateCollision = function(){
    }

    this.OnCollide = function (collider) {
        if (!this.ignoreCollision) {
            if (collider.type == "ball")
                collider.velocity.y = -Math.abs(collider.velocity.y);
            else if (collider.type == "powerup") {
                //create an extra ball
                var newBall = new Ball();
                newBall.position.x = (canvasWidth / 2 - 60);
                newBall.position.y = (canvasHeight - 120);
                currentState.gameObjects.push(newBall);
                currentState.ballCount++;
            }
        }
    }

    this.Draw = function () {
        ctx.drawImage(this.playerImg, this.position.x, this.position.y, this.size.x, this.size.y);
    };
};