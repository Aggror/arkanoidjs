﻿function timestamp() {
    return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
}

var seed = 1;
function Random() {
    var x = Math.sin(seed++) * 10000;
    return x - Math.floor(x);
}