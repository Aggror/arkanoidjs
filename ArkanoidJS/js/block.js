﻿var colorImage = {};
colorImage[0] = "images/element_red_rectangle_glossy.png";
colorImage[1] = "images/element_yellow_rectangle_glossy.png";
colorImage[2] = "images/element_blue_rectangle_glossy.png";
colorImage[3] = "images/element_green_rectangle_glossy.png";
colorImage[4] = "images/element_purple_rectangle_glossy.png";
colorImage[5] = "images/element_grey_rectangle_glossy.png";

function Block() {
    var self = this;
    this.type = "block";
    this.size = { x: 20, y: 20 };
    this.position = { x: 0, y: 0 };
    this.blockImg = new Image();
    this.blockImg.src = "images/element_red_rectangle_glossy.png";
    this.alive = true;
    this.powerUpChance = 0.2;

    this.LoadImageOfColor = function(index){
        this.blockImg.src = colorImage[index];
    }

    this.Update = function () {
    };

    this.OnDeath = function () {
        //if (this.alive)
            //console.log("block destroyed");
        this.alive = false;

        if (Math.random() <= this.powerUpChance) {
            var powerUp;
            if (Math.random() < 0.5)
                powerUp = new PowerUpExtraBall(this.position.x, this.position.y);
            else
                powerUp = new PowerUpFastBall(this.position.x, this.position.y);
            currentState.gameObjects.push(powerUp);
        }
    }

    this.OnCollide = function (collider) {
        this.OnDeath();
    }

    this.UpdateCollision = function () {

    }

    this.Draw = function () {
        ctx.drawImage(this.blockImg, this.position.x, this.position.y, this.size.x, this.size.y);
    };

};